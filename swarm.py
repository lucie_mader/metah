from pyswarm import pso
from math import pi, pow, sin, fabs, sqrt, ceil, floor

lb, ub = [], []
funcs = ["MZ2", "MZ5", "MZ10", "DJF1", "DJF2", "DJF3", \
        "GP", "R", "Z", "SH"]

#n = 2,  0 < xi < pi, i = 1
def MZ2(x):
    ret = 0
    for i, x_i in enumerate(x):
        ret += sin(x_i) * pow(sin((i + 1)* x_i * x_i / pi), 20)

    return -ret;

def MZ5(x):
    t = []
    ret = 0
    for i in x:
        ret += sin(i) * pow(sin(1 * pow(i, 2) / pi), 20)
    return -ret

def MZ10(x):
    t = []
    ret = 0
    for i in x:
        ret += sin(i) * pow(sin(1 * pow(i, 2) / pi), 20)
    return -ret

def DJF1(x):
    ret = 0
    for x_i in x:
        ret += x_i * x_i
    return ret

def DJF2(x):
    return 100 * pow((pow(x[1], 2) - x[0]), 2) + pow((1 - x[0]), 2)

def DJF3(x):
    ret = 0
    for i in x:
        ret += floor(i)
    return ret

def GP(x):
    return (1 + pow(x[0] + x[1] + 1, 2) \
            * (19 - 14 * x[0] + 13 * pow(x[0], 2) \
            - 14 * x[1] + 6 * x[0] * x[1] + 3 * pow(x[1], 2)) \
            * (30 + pow(2 * x[0] - 3 * x[1], 2) * (18 - 32 * x[0] \
            + 12 * pow(x[0], 2) - 48 * x[1] - 36 * x[0] * x[1] + 27 * pow(x[1], 2))))

def R(x):
    ret = 0
    for i in range(len(x) - 1):
        ret += 100 * pow(pow(x[i], 2) - x[i + 1], 2) + (x[i] - 1)
    return ret

def Z(x):
    ret1 = 0
    ret2 = 0
    ret3 = 0

    for i, x_i in enumerate(x):
        ret1 += pow(x_i, 2)
        ret2 += pow(0.5 * i * x_i, 2)
        ret3 += pow(0.5 * i * x_i, 4)

    return ret1 + ret2 + ret3

def SH(x):
    ret = 0
    for i in x:
        ret += -i * sin(sqrt(fabs(i)))
    return ret

if __name__ == "__main__":
    fu = "None"
    while fu not in funcs:
        print "Available functions to minimize:"
        for f in funcs:
            print f
        print
        fu = raw_input("Choose one: ")

    if fu == "MZ2":
        for i in range(10):
            lb.append(0)
            ub.append(pi)
    elif fu == "MZ5":
        for i in range(5):
            lb.append(0)
            ub.append(pi)
    elif fu == "MZ10":
        for i in range(10):
            lb.append(0)
            ub.append(pi)
    elif fu == "DJF1":
        for i in range(5):
            lb.append(-5.12)
            ub.append(5.12)
    elif fu == "DJF2":
        for i in range(2):
            lb.append(-2.048)
            ub.append(2.048)
    elif fu == "DJF3":
        for i in range(7):
            lb.append(-5.12)
            ub.append(5.12)
    elif fu == "GP":
        for i in range(2):
            lb.append(-2)
            ub.append(2)
    elif fu == "R":
        for i in range(2):
            lb.append(-2.048)
            ub.append(2.048)
    elif fu == "Z":
        for i in range(4):
            lb.append(-5)
            ub.append(10)
    elif fu == "SH":
        for i in range(7):
            lb.append(-500)
            ub.append(500)

    xopt, fopt = pso(locals()[fu], lb, ub, \
            maxiter=9000, minfunc=1e-16, minstep=1e-16, \
            swarmsize=460, omega=0.9, phip=0.7, phig=0.2)
    print "Result: " + str(fopt)
    print "Parameters:\n" + str(xopt)
