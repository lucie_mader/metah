import networkx as nx
import matplotlib.pyplot as plt
from random import randrange
import math

def replace(val, expected, replacement):
    if val == expected:
        return replacement
    else:
        return val

def annealing(graph_size):
    G=nx.Graph()
    G.add_nodes_from(range(1, graph_size * 2 + 1))


    for n1 in range(1, 26):
        for n2 in range(1, 26):
            if n1 != n2:
                x1, y1 = replace(n1 % graph_size, 0, 5),\
                        math.ceil(n1 / graph_size)

                x2, y2 = replace(n2 % graph_size, 0, 5),\
                        math.ceil(n2 / graph_size)

                if x1 == x2 and y1 == y2 + 1 or \
                    x1 == x2 and y1 == y2 - 1 or \
                    y1 == y2 and x1 == x2 + 1 or \
                    y1 == y2 and x1 == x2 - 1:
                        print(n1, n2)
                        G.add_edge(n1, n2)


    posl = []
    for j in range(20, -1, -5):
        for i in range(0, 21, 5):
            posl.append((i, j))
    pos = {}
    for i in range(1, 26):
        r = randrange(0, len(posl))
        pos[i] = posl[r]
        del posl[r]

    nx.draw(G, pos, with_labels=True)
    plt.show()

if __name__ == "__main__":
    annealing(5)
